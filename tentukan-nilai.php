<?php

function tentukan_nilai($number)
{
  if ($number >= 85 and $number <= 100) {
    return "Sangat Baik <br />";
  } else if ($number >= 70 and $number < 85) {
    return "Baik <br />";
  } else if ($number >= 60 and $number < 70) {
    return "Cukup <br />";
  } else {
    return "Kurang <br />";
  }
  //  kode disini
  // jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
