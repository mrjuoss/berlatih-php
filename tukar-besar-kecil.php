<?php
function tukar_besar_kecil($string)
{
  $result = "";
  for ($a = 0; $a < strlen($string); $a++) {
    if ($string[$a] == strtoupper($string[$a])) {
      $result .= strtolower($string[$a]);
    } else if ($string[$a] == strtolower($string[$a])) {
      $result .= strtoupper($string[$a]);
    }
  }
  return "Text Semula <b>" . $string . " </b> Menjadi <b>" . $result . "</b><br />";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
