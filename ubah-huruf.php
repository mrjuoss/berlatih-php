<?php
function ubah_huruf($string)
{
  $alfabet = "abcdefghijklmnopqrstuvwxyz";
  $result = "";
  for ($a = 0; $a < strlen($string); $a++) {
    if ($string[$a] === "z") {
      $result .= "a";
    } else {
      for ($b = 0; $b < strlen($alfabet); $b++) {
        if ($string[$a] === $alfabet[$b]) {
          $result .= $alfabet[$b + 1];
        }
      }
    }
  }
  return "Text Awal <b>" . $string . "</b> karakter setelahnya adalah <b>" . $result . "</b><br />";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
